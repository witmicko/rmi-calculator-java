/*
Filename: HelloWorld.java
*/

import java.rmi.Remote;
import java.rmi.RemoteException;

/*

Classname: CalculatorInterface
Comment: The remote interface.

*/

public interface CalculatorInterface extends Remote {
	String helloWorld() throws RemoteException;
    String clientConnect()throws RemoteException;

    float add(float x, float y)throws RemoteException;
    float sub(float x, float y)throws RemoteException;
    float mul(float x, float y)throws RemoteException;
    float div(float x, float y)throws RemoteException;


}
