/*
Filename: HelloWorldServer.java
*/

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.rmi.server.ServerNotActiveException;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.registry.LocateRegistry;
import java.util.ArrayList;


// If you don't run the rmiregistry application from the command line
// include the following import:
// import java.rmi.registry.LocateRegistry.Registry;

/*
Classname: CalculatorServer
Purpose: The RMI server.
*/

public class CalculatorServer extends UnicastRemoteObject implements CalculatorInterface {
    private int clientCtr;
    private ServerGui gui;
    private ScriptEngine engine;

    public CalculatorServer() throws RemoteException {
        super();
        gui = new ServerGui();
        gui.setVisible(true);
        clientCtr = 0;
        ScriptEngineManager mgr = new ScriptEngineManager();
        engine = mgr.getEngineByName("JavaScript");
    }

    public String helloWorld() {
        System.out.println("Invocation to helloWorld was succesful!");
        return "Hello World from RMI server! yolo shhhwaaaag";
    }

    @Override
    public String clientConnect() {
        clientCtr++;
        String clientName = "Client-" + clientCtr;
        try {
            System.out.println(getClientHost());
            gui.systemMessageAppend(clientName + " connected at IP: \n" + getClientHost());
        } catch (ServerNotActiveException e) {
            e.printStackTrace();
        }
        return clientName;
    }

    @Override
    public float add(float x, float y) throws RemoteException {
        return x + y;
    }

    @Override
    public float sub(float x, float y) throws RemoteException {
        return x - y;
    }

    @Override
    public float mul(float x, float y) throws RemoteException {
        return x * y;
    }

    @Override
    public float div(float x, float y) throws RemoteException {
        return x / y;
    }


    public static void main(String args[]) {
        try {
            // Create an object of the CalculatorServer class.
            CalculatorServer obj = new CalculatorServer();
            // Bind this object instance to the name "HelloServer".
            // Include the following line if rmiregistry was not started on the command line
            Registry registry = LocateRegistry.createRegistry(1099);

            // and replace the Naming.rebind() with the next line
            registry.rebind("CalculatorInterface", obj);

            Naming.rebind("CalculatorInterface", obj);

            System.out.println("CalculatorInterface bound in registry");
        } catch (Exception e) {
            System.out.println("CalculatorServer error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void run() {

    }
}