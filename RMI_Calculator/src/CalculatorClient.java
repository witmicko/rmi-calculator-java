/*
Filename: HelloWorldClient.java
*/

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Arc2D;
import java.rmi.Naming;
import java.rmi.RemoteException;

/*
Classname: CalculatorClient
Comment: The RMI client.
*/

public class CalculatorClient {
    private CalculatorGui gui;
    private String clientName;
    private static CalculatorInterface obj = null;


    public CalculatorClient(){
        gui = new CalculatorGui();

    }

    public static void main(String args[]) {
        CalculatorClient client = new CalculatorClient();
        client.run();

    }

    private void run() {
        try {
            obj = (CalculatorInterface) Naming.lookup("CalculatorInterface");
            clientName = obj.clientConnect();
            System.out.println("client name " + clientName);
            gui.setWindowTitle(clientName);
            gui.setSubmitBtnListener(new SubmitBtnListener());
            gui.setVisible(true);

        } catch (Exception e) {
            System.out.println("CalculatorClient exception: "
                    + e.getMessage());
            e.printStackTrace();
        }

    }

    private class SubmitBtnListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            String input = gui.getInputText()
                    .replace("/", " / ")
                    .replace("*", " * ")
                    .replace("-", " - ")
                    .replace("+", " + ");
            String[]inputArr = input.split(" ");
            if(inputArr.length != 3)gui.setInputText("Invalid operation");
            else{
                float x = Float.parseFloat(inputArr[0]);
                float y = Float.parseFloat(inputArr[2]);
                float result = 0f;
                try {
                    switch (inputArr[1]) {
                        case "/":
                            result = obj.div(x, y);
                            break;
                        case "*":
                            result = obj.mul(x, y);
                            break;
                        case "-":
                            result = obj.sub(x, y);
                            break;
                        case "+":
                            result = obj.add(x, y);
                            break;
                    }
                    gui.systemMessageAppend("Recieved Data from server");
                    gui.setInputText(prettyPrint(result));
                }catch (RemoteException remoteException){
                    gui.systemMessageAppend("error: \n" + remoteException.getMessage());
                }
            }
        }
    }

    private static String prettyPrint(float d) {
        long i = (long) d;
        return d == i ? String.valueOf(i) : String.valueOf(d);
    }
}