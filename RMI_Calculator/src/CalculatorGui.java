
import java.awt.*;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.border.LineBorder;
import java.util.ArrayList;

public class CalculatorGui extends JFrame {
    private final static int HEIGHT = 420;
    private final static int WIDTH  = 250;
    private final static int BTN_SIZE  = 53;


    private JPanel contentPane;
    private JTextField inputField;
    private JTextArea systemMessage;
    private JButton submitBtn;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {

        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    CalculatorGui frame = new CalculatorGui();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public CalculatorGui() {
        setTitle("Asasdad");
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(600, 100, WIDTH, HEIGHT);
        contentPane = new JPanel();
        contentPane.setBorder(new LineBorder(new Color(0, 0, 0), 5, true));
        setContentPane(contentPane);
        contentPane.setLayout(null);


        inputField = new JTextField();
        inputField.setBackground(new Color(144, 238, 144));
        inputField.setFont(new Font("Tahoma", Font.BOLD, 18));
        inputField.setBounds(10, 10, BTN_SIZE * 4 , 60);
        contentPane.add(inputField);
        inputField.setColumns(10);

        ArrayList<JButton>buttons = new ArrayList<>();
        submitBtn = new JButton("Submit");

        buttons.add(new JButton("/"));
        buttons.add(new JButton("*"));
        buttons.add(new JButton("-"));
        buttons.add(new JButton("+"));
        buttons.add(new JButton("7"));
        buttons.add(new JButton("4"));
        buttons.add(new JButton("1"));
        buttons.add(new JButton("0"));
        buttons.add(new JButton("8"));
        buttons.add(new JButton("5"));
        buttons.add(new JButton("2"));
        buttons.add(submitBtn);
        buttons.add(new JButton("9"));
        buttons.add(new JButton("6"));
        buttons.add(new JButton("3"));

        int idx = 0;
        BtnListener btnListener = new BtnListener();
        for (int i = 0; i < 4; i++) {
            for (int j = 1; j < 5; j++) {
                JButton btn = buttons.get(idx);

                if (btn.equals(submitBtn)) {
                    btn.setBounds(10 + i * BTN_SIZE, 20 + j * BTN_SIZE, 2 * BTN_SIZE, BTN_SIZE);
                    btn.setBackground(Color.GREEN);
                    btn.setForeground(Color.BLACK);
                } else {
                    btn.setBounds(10 + i * BTN_SIZE, 20 + j * BTN_SIZE, BTN_SIZE, BTN_SIZE);
                    btn.setFont(new Font("Tahoma", Font.BOLD, 11));

                    btn.addActionListener(btnListener);
                }
                contentPane.add(btn);
                idx++;
                if (idx >= buttons.size()) break;
            }

        }

        systemMessage = new JTextArea();
        int textAreaY = 10 + buttons.get(3).getBounds().y + BTN_SIZE;
        systemMessage.setBounds(10, textAreaY, BTN_SIZE * 4, 80);
        contentPane.add(systemMessage);
    }

    public void systemMessageAppend(String msg){
        systemMessage.append(msg);
    }

    public String getInputText(){
        return inputField.getText();
    }

    public void setInputText(String text){
        inputField.setText(text);
    }

    public void setSubmitBtnListener(ActionListener listener){
        submitBtn.addActionListener(listener);
    }

    public void setWindowTitle(String title){
        setTitle(title);
    }

    private class BtnListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            JButton btn = (JButton)e.getSource();
            String text = inputField.getText();
            inputField.setText(text + btn.getText());
        }
    }
}
