# Assignment #3: Remote Method Invocation

Using Exercise 1 from the in-class exercise develop a Java RMI distributed application to replicate the Client Side GUI demonstrated below.

The Client requests one of the calculator methods provided by the Server through the registry services provided.

##### Procedure:
- Client selects two integer operands and one operator from calculator.
- Submit request to Server to invoke a chosen method using "Submit" button
- Server returns result to Client where it is output to textfield.
- Provide appropriate system messages from client-to-server and server-to-client.

